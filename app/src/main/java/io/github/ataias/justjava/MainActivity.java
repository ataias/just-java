package io.github.ataias.justjava;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

/*
 * This app displays an order form to order coffee
 */

public class MainActivity extends AppCompatActivity {

    // MARK: Properties
    private int minQuantity = 1;
    private int maxQuantity = 100;

    int quantity = 2;
    int price = 5;
    boolean hasWhippedCream = false;
    boolean hasChocolate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //This method displays the given quantity value on the screen
    /*
    private is a Java keyword which declares a member's access as private.
    That is, the member is only visible within the class, not from any other
    class (including subclasses). The visibility of private members extends
    to nested classes.
     */
    private void displayQuantity(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        String s = "" + number;
        quantityTextView.setText(s);
    }

    /**
     * This method displays the given price on the screen.
     */
    private void displayPrice(int number) {
        TextView priceTextView = (TextView) findViewById(R.id.price_text_view);
        priceTextView.setText(NumberFormat.getCurrencyInstance().format(number));
    }

    /*
    * This method displays the given text on the screen.
    */
    private void displayMessage(String message) {
        TextView priceTextView = (TextView) findViewById(R.id.price_text_view);
        priceTextView.setText(message);
    }

    // MARK: Actions

    /*This method is called when the order button is called
    *
    * @params view is the button that was pressed
    * */
    public void submitOrder(View view) {
        EditText nameText = (EditText) findViewById(R.id.name_edit_text);
        String name = nameText.getText().toString();
        int pricePerCup = price;
        if (hasWhippedCream) pricePerCup += 1;
        if (hasChocolate) pricePerCup += 2;
        String summary = createOrderSummary(name, pricePerCup, hasWhippedCream, hasChocolate);
        displayMessage(summary);

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"coffeeshop@coffeeshop.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "My order");
        intent.putExtra(Intent.EXTRA_TEXT, summary);
        if (intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        } else {
            Toast.makeText(this, "Mail application not found", Toast.LENGTH_LONG).show();
        }
    }

    //This method increments the number of coffees
    public void increment(View view) {
        if (quantity == maxQuantity){
            Toast.makeText(this, "Maximum order", Toast.LENGTH_SHORT).show();
            return;
        }
        quantity++;
        displayQuantity(quantity);
    }

    //This method decrements the number of coffees
    public void decrement(View view) {
        if (quantity == minQuantity){
            Toast.makeText(this, "Minimum order", Toast.LENGTH_SHORT).show();
            return;
        }
        quantity--;
        displayQuantity(quantity);
    }

    /*
    * This method updates the hasWhippedCream when the checkbox is pressed
     */

    public void onCheckBoxButtonClick(View view) {
        CheckBox checkBox = (CheckBox) view;
        if (checkBox.getId() == R.id.chocolate) {
            hasChocolate = checkBox.isChecked();
            Log.i("MainActivity.java", "Chocolate = " + hasChocolate);
            return;
        }

        if (checkBox.getId() == R.id.whipped_cream) {
            hasWhippedCream = checkBox.isChecked();
            Log.i("MainActivity.java", "hasWhippedCream = " + hasWhippedCream);
            return;
        }


    }

    private String createOrderSummary(String name, int price, boolean addWhippedCream, boolean addChocolate) {
        String message = "Name: " + name + "\n";
        message += "Add whipped cream? " + hasWhippedCream + "\n";
        message += "Add chocolate? " + hasChocolate + "\n";
        message += "Quantity: " + quantity + "\n";
        message += "Total: $" + quantity * price + "\n";
        message += "Thank you!";
        return message;
    }
}
